const express = require('express')
const app = express()
const port = 3000

endpoints = []
for (i = 0; i < 1000; i++){
    endpoints.push(Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15))
}

app.get('/', function (req, res) {
  link = "<a href=\"" + endpoints[0] + "\">click here, if you are patient enough I will give you the flag</a>"
  res.send(link)
})

app.get("/:val", function (req, res) {
	val = req.params.val
  index = endpoints.indexOf(val)

  if (index == 998) {
    res.status(200).send("Nice clicking, I'm very impressed! Now to go onwards and upwards! <br/><pre>dam{I_hope_you_did_this_manually}</pre>")
  } else {
    link = "<a href=\"" + endpoints[index+1] + "\">click here, if you are patient enough I will give you the flag</a>"
    res.status(200).send(link)
  }
})

console.log(endpoints[988])

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})
