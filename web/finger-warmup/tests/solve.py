#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup


url = "https://finger-warmup.chals.damctf.xyz/"


def main():
    path = ""
    while True:
        r = requests.get(url + path)
        soup = BeautifulSoup(r.text, 'html.parser')
        print(soup)
        try:
            path = soup.find("a")["href"]
        except TypeError:
            break


if __name__ == "__main__":
    main()
