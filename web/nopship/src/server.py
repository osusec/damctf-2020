from flask import Flask, render_template, request, make_response
from markupsafe import Markup 
import os, binascii
import jwt
import random

app = Flask(__name__)

charset = 'abcdefghijklmnopqrstuvwxyz0123456789{}'
jwtsecret = 'i_h0p3_1_didnt_acc1d3nt4lly_c0mmit_th1s'

@app.route('/')
def index():
    secret = request.cookies.get('secret') 
    token = request.cookies.get('token') 

    correct = secret == 'what a fantastic secret value!123'

    if not token:
        payload = {
            'id': random.randint(1, 10000000),
            'admin': correct,
        }

        token = jwt.encode(payload, jwtsecret, algorithm='HS256').decode()


    decoded = jwt.decode(token, jwtsecret, algorithm='HS256')
    isadmin = decoded['admin']

    style = Markup.escape(request.args.get('style'))
    token_esc = Markup.escape(token)
    resp = make_response(render_template('index.html', style=style, secret=token_esc, admin=isadmin))
    resp.set_cookie('token', token) 

    return resp
