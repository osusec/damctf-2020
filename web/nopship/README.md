# nopship

Description:

```
Reading elements using CSS attributes. Recursively including CSS files
to read JWT in one go.
```

Flag:

```
dam{th4nkz_f0rr_th3_h3lp}
```

## Checklist for Author

* [~] Challenge compiles
* [~] Dockerfile has been updated
* [+] There is at least one test exploit/script in the `tests` directory
