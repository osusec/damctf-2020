"use strict";

const _ = require('lodash')
const MongoClient = require('mongodb').MongoClient;
const url = '<REDACTED>';
const toInsert = require("./db_gql_db.json");

class Database {
    constructor() {
        this.client = null;
    }

    async connect(collection_name) {
        if (_.isNull(this.client)) {
            this.client = MongoClient(url);
            try {
                await this.client.connect();
            } catch (e) {
                console.log(e);
                throw `Failed connecting to db: ${e}`;
            }
        }

        const db = this.client.db("gqp_db");
        const collection = db.collection(collection_name);

        return collection;
    }
    close() {
        if (!_.isNull(client)) {
            this.client.close();
        }
    }

}

//TODO: Index
const client = new Database();
let collection;
client.connect("planetarium").then((col) => {
    collection = col;
    return collection.deleteMany({});
}).then(() => {
    return collection.insertMany(toInsert);
}).then(() => {
    return collection.ensureIndex("eclipse");
}).then(() => {
    return collection.ensureIndex({"date.day": 1, "date.month": 1, "date.year": 1, "date.era": 1});
}).then(() => {
    return client.close();
})
