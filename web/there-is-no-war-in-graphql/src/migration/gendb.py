#!/usr/bin/env python3

import datetime
import json
import random

# {id: 0, date: {day: 29, month: 9, year: 100, era: "AG"}, eclipse: false},

def main():
    dates = []
    start_date = datetime.date(99, 1, 1)
    end_date = datetime.date(180, 1, 4)
    delta = datetime.timedelta(days=1)

    i = 0
    while start_date <= end_date:
        obj = {"id": i,
               "date": {"day": start_date.day, "month": start_date.month, "year": start_date.year, "era": "AG" },
               "eclipse": False,
               }
        start_date += delta
        i += 1
        dates.append(obj)


    for _ in range(7):
        ecl = random.choice(dates)
        ecl['eclipse'] = True

    print(json.dumps(dates))

if __name__ == "__main__":
    main()
