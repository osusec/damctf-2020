"use strict";

const express = require('express');
const mongo_helper = require("./mongo_helper.js");
const _ = require("lodash");

const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');
const { GraphQLJSON, GraphQLJSONObject } = require('graphql-type-json');


// Construct a schema, using GraphQL schema language
let schema = buildSchema(`
  scalar JSON
  scalar JSONObject

  type Date_c {
    day: Int
    month: Int
    year: Int
    era: String
  }

  input Date_i {
    day: Int
    month: Int
    year: Int
    era: String
  }

  input BatchSearch {
    options: JSON
    search: [PlanetariumSearch]
  }

  input PlanetariumSearch {
    eclipse: Boolean
    date: Date_i
  }

  type BatchPlanetarium {
    data: [Planetarium]
    log: String
  }

  type Planetarium {
    eclipse: Boolean
    date: Date_c
  }

  type Query {
    planetariumBatch(lookup: BatchSearch!, debug: Boolean): BatchPlanetarium
    planetariumOne(date: Date_i!): Planetarium
    planetariumToday: Planetarium
    invade(date: Date_i!): String
  }
`);

class Date_c {
    constructor({day, month, year, era}) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.era = era;
    }
}

class Planetarium {
    constructor({date, eclipse}) {
        this.eclipse = eclipse;
        if (!_.isNil(date))
            this.date = new Date_c(date);
    }
}

// The root provides the top-level API endpoints
let root = {
  planetariumBatch: async (agg, debug) => {
    const [batch_result, log] = await mongo_helper.planetariumBatch(agg.lookup, debug);
    let data = batch_result.map(result => new Planetarium(result));
    return {data: data, log: log};
  },
  planetariumOne: async (date) => {
    return new Planetarium(await mongo_helper.planetariumOne(date));
  },
  planetariumToday: async () => {
    return new Planetarium(await mongo_helper.planetariumToday());
  },
  invade: async (date) => {
    return await mongo_helper.invade(date.date);
  },
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject,
}

let app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.use('/', (req, res, next) => {
  return res.redirect(301, "/graphql")
})

app.listen(8080, '0.0.0.0');
console.log('Running a GraphQL API server at 0.0.0.0:8080/graphql');
