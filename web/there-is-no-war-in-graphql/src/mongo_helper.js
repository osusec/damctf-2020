"use strict";

const _ = require('lodash');
const MongoClient = require('mongodb').MongoClient;
const uri = '';
const util = require('util');
const fs = require("fs");

const flag = fs.readFileSync("flag");

class Database {
    constructor() {
        this.client = null;
    }

    async connect(collection_name) {
        if (_.isNull(this.client)) {
            this.client = MongoClient(uri);
            try {
                await this.client.connect();
            } catch (e) {
                console.log(e);
                throw `Failed connecting to db: ${e}`;
            }
        }

        const db = this.client.db("gqp_db");
        const collection = db.collection(collection_name);

        return collection;
    }
    close() {
        if (!_.isNull(client)) {
            this.client.close();
        }
    }
}

const client = new Database();

async function planetariumBatch(p_i, debug) {
    const collection = await client.connect("planetarium");
    let command = {}
    if (p_i.search) {
        command = {
            $match: {
                $or: [
                    {date: {$in: p_i.search.map(x => x.date)}},
                    {eclipse: {$in: p_i.search.map(x => x.eclipse)}}
                ]
            }
        };
    }
    else {
        command = {$match: {}};
    }
    let agg = [command, {$project: {"date": 0}}];
    if (p_i.options) {
        if (_.isArray(p_i.options)) {
            for (const option of p_i.options) {
                agg.push(option);
            }
        }
        else if (_.isObject(p_i.options)) {
                agg.push(p_i.options);
        }
    }

    const found = await collection.aggregate(agg).toArray();
    if (found.length > 5) {
        throw new Error("Woah woah! That's far too many dates to calculate!");
    }
    let log = null;
    if (debug) {
        log = `Successful query from collection 'planetarium'. Final query: ${JSON.stringify(agg)}`;
    }

    return [found, log];
}

async function planetariumOne(date) {
    const collection = await client.connect("planetarium");
    const found =  await collection.findOne(date);
    if (_.isNil(found)) {
        throw new Error("Entry not found");
    }
    return found;
}

async function planetariumToday() {
    const date = {date: {"day": 1, "month": 1, "year": 99, "era": "AG"}};
    return await planetariumOne(date);
}

async function invade(date){ 
    const collection = await client.connect("planetarium");
    const found = await collection.findOne({date});
    let result = "";
    if (_.isNil(found)) {
        result = "Error: invalid date";
    }
    else if (found.eclipse == true) {
        result = `Invasion failed anyway, but at least we got the flag: ${flag}`;
    } else {
        result = "Can only invade when there's an eclipse: on the day of black sun.";
    }
    return result;
}

exports.planetariumOne = planetariumOne;
exports.planetariumBatch = planetariumBatch;
exports.planetariumToday = planetariumToday;
exports.invade = invade;
