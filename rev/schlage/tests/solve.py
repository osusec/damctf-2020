#!/usr/bin/env python3

from pwn import *

# p = process("../schlage")
p = remote("chals.damctf.xyz", 31932)

# Pins must be opened in this order:
# 3, 1, 5, 2, 4

############# PIN 3 ############# 

p.recvuntil("Which pin would you like to open?")
p.sendline("3")

# 3449466328
pin1 = str(0xdeadbeef ^ 0x13371337)
p.sendline(pin1)

############# PIN 1 ############# 

p.recvuntil("Which pin would you like to open?")
p.sendline("1")

values = [0x3e, 0x57, 0x81, 0xd3, 0x25, 0x93]
goal = 0xee
out = goal
for i in values:
    out ^= i

# 99
p.sendline(str(out))

############# PIN 5 ############# 

p.recvuntil("Which pin would you like to open?")
p.sendline("5")

# 1413036362
rand = process("./pin5").recvall(timeout=0.2)
p.sendline(rand)

############# PIN 2 ############# 

p.recvuntil("Which pin would you like to open?")
p.sendline("2")

p.recvline()
p.recvline()
p.recvline()

seed = p.recvline()
argv = ["./pin2", seed]
rand = process(argv=argv).recvall(timeout=0.2)
p.sendline(rand)

############# PIN 4 ############# 

p.recvuntil("Which pin would you like to open?")
p.sendline("4")

argv = ["./pin4", seed]
rand = process(argv=argv).recvall(timeout=0.2)
key = 0x41 + (int(rand) % 10)

goal = 0x123
current = 0
the_str = ""

pad = chr(key+11)
while True:
    if current + (ord(pad) ^ key) <= goal:
        the_str += pad
        current += (ord(pad) ^ key)
    else:
        break

if current != goal:
    the_str += chr((goal - current) ^ key)

p.sendline(the_str)
print(p.recvall(timeout=1).decode())