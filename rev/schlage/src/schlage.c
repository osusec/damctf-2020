#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define NUM_PINS 5
bool pins[NUM_PINS] = {0}; // false is locked, true is unlocked
int dependencies[NUM_PINS] = {3, 5, 0, 2, 1}; // order: 3, 1, 5, 2, 4
// int dependencies[NUM_PINS] = {0}; // for testing

bool is_lock_unlocked() {
    for (int i = 0; i < NUM_PINS; i++) {
        if (!pins[i]) {
            return false;
        }
    }

    return true;
}

int get_int() {
    char input[20];
    int val;

    printf("> ");
    fgets(input, 20, stdin);
    val = atoi(input);

    if (!val) {
        puts("Bad input!");
        exit(0);
    }

    return val;
}

void print_lock() {
    bool unlocked;

    unlocked = is_lock_unlocked();

    puts("");

    if (unlocked) {
        puts("                        ------------");
        puts("                       /            \\");
        puts("                      /   --------   \\");
        puts("                     /   /        \\   \\");
        puts("                    /   /          \\   \\");
    } else {
        puts("          ------------");
        puts("         /            \\");
        puts("        /   --------   \\");
        puts("       /   /        \\   \\");
        puts("      /   /          \\   \\");
    }

    puts("    ========================");
    puts("    |                      |");

    for (int i = 0; i < NUM_PINS; i++) {
        if (pins[i]) {
            printf("    |    |===>   <====|    |");
        } else {
            printf("    |    |=====><=====|    |");
        }
        printf("  Pin %d\n", i+1);
    }    

    puts("    |                      |");
    puts("    ========================");
    puts("");
}

int get_pin() {
    int choice = -1;

    puts("Which pin would you like to open?");

    while (true) {
        if (choice != -1) puts("Invalid pin!");

        choice = get_int();
        if (choice >= 1 && choice <= NUM_PINS) break;
    }

    return choice;
}

void print_flag() {
    FILE *fp;
    char flag[0x40] = {0};
    fp = fopen("./flag", "r");
    fgets(flag, 0x40, fp);
    puts(flag);
    fclose(fp);
}

void do_pin3() {
    unsigned int num1, input;
    
    if (dependencies[2] && !pins[dependencies[2]-1]) {
        puts("Hmm, this pin won't budge!");
        return;
    }

    num1 = 0xdeadbeef;

    puts("Give me a number!");
    input = get_int();

    if ((num1 ^ input) == (unsigned int)0x13371337) {
        pins[2] = true;
        puts("Great!");
    } else {
        puts("Nope!");
    }
}

void do_pin1() {
    unsigned char values[] = {0x3e, 0x57, 0x81, 0xd3, 0x25, 0x93};
    unsigned char input;
    int i;
    
    if (dependencies[0] && !pins[dependencies[0]-1]) {
        puts("Hmm, this pin won't budge!");
        return;
    }

    puts("Number please!");
    input = (unsigned char)(get_int() & 0xff);

    unsigned char final = 0;
    final ^= input;
    for (i = 0; i < 6; i++) {
        final ^= values[i];
    }

    if (final == 0xee) {
        pins[0] = true;
        puts("Great!");
    } else {
        puts("Nope!");
    }
}

void do_pin5() {
    int num;

    if (dependencies[4] && !pins[dependencies[4]-1]) {
        puts("Hmm, this pin won't budge!");
        return;
    }

    srand(0x42424242);

    puts("I bet you can't guess my random number!");
    num = get_int();

    if (num == rand()) {
        puts("Wow! That was some impressive guessing.");
        pins[4] = true;
    } else {
        puts("Good try!");
    }
}

void do_pin2() {
    int time_val, input;

    if (dependencies[1] && !pins[dependencies[1]-1]) {
        puts("Hmm, this pin won't budge!");
        return;
    }

    time_val = time(NULL);

    puts("Hmm, a little piece of paper just fell out of the lock with some random numbers on it!");
    puts("I wonder what it means?");
    printf("%d\n", time_val);

    srand(time_val);

    puts("What's your favorite number?");
    input = get_int();

    if (input == rand()) {
        puts("Woah, that's the lock's favorite number too! Small world, eh?");
        pins[1] = true;
    } else {
        puts("To each their own I guess!");
    }
}

void do_pin4() {
    char input[32] = {0};
    int key, total, len;

    if (dependencies[3] && !pins[dependencies[3]-1]) {
        puts("Hmm, this pin won't budge!");
        return;
    }

    puts("What's your favorite sentence?");
    fgets(input, 32, stdin);

    input[strcspn(input, "\n")] = 0;

    key = 'A' + (rand() % 10);
    total = 0;
    len = strlen(input);
    for (int i = 0; i < len; i++) {
        total += input[i] ^ key;
    }

    if (total == 0x123) {
        puts("Such a cool sentence!");
        pins[3] = true;
    } else {
        puts("Not a big fan of that sentence");
    }
}

int main() {
    int pin_choice;

    puts("*** WELCOME TO THE SCHLAGE 9000 ***");
    puts("***    AUTHORIZED USERS ONLY    ***");

    while (!is_lock_unlocked()) {
        print_lock();

        pin_choice = get_pin();
        switch (pin_choice) {
            case 1:
                do_pin1();
                break;
            case 2:
                do_pin2();
                break;
            case 3:
                do_pin3();
                break;
            case 4:
                do_pin4();
                break;
            case 5:
                do_pin5();
                break;
        }
    }

    print_lock();

    puts("Congratulations on opening the lock! I really needed to get back into my apartment.");
    puts("Here, have a flag for your troubles:");
    print_flag();

    return 0;
}