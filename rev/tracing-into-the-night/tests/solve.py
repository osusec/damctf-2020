#!/usr/bin/python3

from subprocess import run, PIPE
import re

#   4013c0:       48 39 c2                cmp    %rax,%rdx
#   4013c3:       75 40                   jne    401405 <main+0x1cf>

d = ''
prev = ''
with open('../src/trace', 'r') as f:
    for line in f:
        if '4013c3:' in prev:
            if '4013c5:' in line:
                d += '1'
            elif '401405:' in line:
                d += '0'
        prev = line

d = int(d, 2)
p = run(['../tracing-into-the-night'], stdout=PIPE,
        input=str(d)+'\n', encoding='ascii')
flag = re.search('dam{.*}', p.stdout).group(0)
print('Flag: {}'.format(flag))
