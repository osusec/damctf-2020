# tracing-into-the-night

Description:

```
Given a program that does square and multiply algorithm for modular exponentiation to decrypt flag.
Given a trace of an execution that gives the flag. Use the data dependency to leak `d` and get flag.


```

Flag:

```
dam{and_1nto_d4ta_depend3ncy}
```

## Checklist for Author

* [+] Challenge compiles
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [+] There is at least one test exploit/script in the `tests` directory
