# cbc-padding-oracle

Description:

```
    You will be provided with an AES-CBC encryptor source code.

    The code let you get the encrypted text of,
    AES_CBC_ENC(A*64 + flag + your_choice_of_input).

    Also, the code let you check if your encrypted text is intact,
    in other words, it will decrypt your ciphertext input and
    will check the padding for the sainty check.

    Under this condition, can you launch the padding oracle attack
    to this program to leak the flag?

    https://en.wikipedia.org/wiki/Padding_oracle_attack

```

Flag:

```
dam{_Or4Cl3_}
```

## Checklist for Author

* [+] Challenge compiles
* [+] Dockerfile has been updated
* [+] `container_src/run_chal.sh` has been updated
* [+] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [+] There is at least one test exploit/script in the `tests` directory
* [+] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
