#!/usr/bin/python3

from Crypto.Util.number import bytes_to_long as b2l
import os
import socket
import threading
import subprocess
import secrets

CHUNK = 10000

with open("flag", 'rb') as f:
    flag = f.read()

def get_rand(p):
    return secrets.randbelow(p-1) + 1

class Server:
    def __init__(self, host='0.0.0.0', port=31337):
        print(f"Binding to {host}:{port}")
        self.s = socket.socket()
        self.s.bind((host, port))

    def listen(self):
        self.s.listen(5)
        while True:
            c, addr = self.s.accept()
            threading.Thread(target=self.puzzle, args=(c,)).start()

    def gen_keys(self):
        p = 91514892819797033705179399571227611692092856913300377973266729140373382315305352325947520546767428375542736209947536010920931094235256987366602022333668598445943676897129270112929569042756146444855210872641963540227487080469555040640668407120910156044105141128559028329041777867127037483908930283653500859907442430934763996059464170608731760771244982499063459386776776115639036656414141326882715090187673746563880543318956483611125878045563045085767103391255891846764748289144269428758133873311885447854229528568813923719146212759974019867134361918383584469265829718289574757937412545468378304435902459
        g = get_rand(p)
        skey = get_rand(p)
        return g, p, skey

    def puzzle(self, conn):
        def receive_ints(c):
            received = c.recv(CHUNK).decode().strip("\n").split(",")
            return [int(x) for x in received]

        try:
            share0 = os.urandom(len(flag))
            share1 = bytes(a ^ b for a, b in zip(flag, share0))
            shares = [b2l(share0), b2l(share1)]

            c = conn
            number = 0
            outgoing = '''*** Welcome to my Oblivious Transfer server! ***

I have cleverly split the flag into 2 secret shares, and will by happy to give you one of them.
You will have to xor both shares together to obtain the flag, but you stand no chance!
This oblivious transfer protocol will only allow you to receive one of them :)
            '''
            c.send(outgoing.encode('UTF-8'))
            g, mod, skey = self.gen_keys()
            c.send(f"{g},{mod},{pow(g, skey, mod)}".encode())
            keys = receive_ints(c)[:2]
            if keys[0] == keys[1]:
                c.send(b"What are you doing sending me identical keys? You trying to decrypt both shares or something? This incident will be reported!!")
                c.shutdown(1)
                c.close()
                return

            ctexts = [(pow(key, skey, mod) * shares[i]) % mod for i, key in enumerate(keys)]

            formatted_ctexts = ','.join(str(c) for c in ctexts).encode()
            c.send(formatted_ctexts)

            c.shutdown(1)
            c.close()
        except:
            pass

if __name__ == '__main__':
    server = Server()
    server.listen()
