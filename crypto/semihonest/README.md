# semihonest

Description:

```
OT is secure
I hope you will not break it
Don't be malicious
```

Flag:

```
dam{I_thought_y0u_w3r3_h0n3st}
```

## Checklist for Author

* [X] Challenge compiles
* [X] Dockerfile has been updated
* [X] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [X] There is at least one test exploit/script in the `tests` directory

## Info

_Author, put whatever you want here_
