# guess-secret

Description:

```
  Can you break my super secure and efficient communication method
  over the internet?

  This challenge is inspired by documented attacks to TLS/HTTPS
  data compression before encryption, namely, BEAST, CRIME, BREACH, and HEIST.

```

Flag:

```
dam{9f64ee1d4a7d6d8fe9136c3e9a74fc76}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
