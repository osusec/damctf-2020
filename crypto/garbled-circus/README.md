# garbled-circus

Description:

```
To address privacy concerns with our scoreboard, we decided to release a tool to allow players to check their flags privately.

Note: Our solution takes a few hundred separate connections to recover the flag, and completes in several minutes.
```

Flag:

```
dam{0NE_moRe_M@LiciOU5_a<T-in-7he_G@rB1ED-<1RCuS}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
