use crate::garbler::Garbler;
use fancy_garbling::{
    errors::TwopacError, twopac::semihonest::PartyId, Fancy, FancyInput, FancyReveal,
};
use ocelot::ot::KosSender as OtSender;
use scuttlebutt::{AbstractChannel, AesRng};
use std::{
    io,
    io::{Error, ErrorKind, Write},
    net::TcpStream,
    process::exit,
    str::from_utf8,
};

mod garbler;

fn equality_check<G: Fancy>(
    gb: &mut G,
    xs: &[G::Item],
    ys: &[G::Item],
) -> Result<G::Item, G::Error> {
    if xs.len() != ys.len() {
        return gb.constant(0, 2);
    }

    let bits_same = xs
        .iter()
        .zip(ys.iter())
        .map(|(x, y)| {
            let different = gb.xor(x, y)?;
            gb.negate(&different)
        })
        .collect::<Result<Vec<G::Item>, G::Error>>()?;
    gb.and_many(&bits_same)
}

pub fn get_len<C: AbstractChannel>(mut channel: C) -> io::Result<usize> {
    Ok(channel.read_u64()? as usize)
}

pub fn leak_bit<C: AbstractChannel>(
    mut channel: C,
    bit: usize,
    len: usize,
) -> Result<bool, TwopacError> {
    let other_len = channel.read_u64()?;
    channel.write_u64(len as u64)?;
    channel.flush()?;
    if other_len != len as u64 {
        return Ok(false);
    }

    let mut gb = Garbler::<C, AesRng, OtSender>::new(channel, AesRng::new())?;

    // Only check the 1 bit, ignoring all of the others.
    let muls = (0..8*len)
        .map(|i| if i == bit { 1 } else { 0 })
        .collect::<Vec<u16>>();

    let moduli = vec![2; 8 * len];
    let xs = gb.encode_many(&vec![0; 8 * len], &moduli)?;
    let ys = gb.receive_many_malicious(PartyId::Evaluator, &moduli, &muls)?;

    let eq = equality_check(&mut gb, &xs, &ys)?;
    let eq_revealed = gb.reveal(&eq)?;
    Ok(eq_revealed == 0)
}

fn main() -> io::Result<()> {
    let args = std::env::args().collect::<Vec<String>>();
    if args.len() != 2 {
        println!("Usage:");
        println!("{} <host>:<port>", args[0]);
        exit(1);
    }

    let channel = TcpStream::connect(&args[1])?;
    let len = get_len(channel)?;
    println!("Flag length: {}", len);

    println!("{}|", " ".repeat(8 * len));

    let mut flag = vec![0; len];
    for i in 0..8*len {
        let channel = TcpStream::connect(&args[1])?;
        if leak_bit(channel, i, len).map_err(|e| Error::new(ErrorKind::Other, e))? {
            flag[i / 8] |= 1 << (i % 8);
        }
        print!("*");
        io::stdout().flush()?;
    }
    println!("\n");

    let flag = from_utf8(&flag).map_err(|e| Error::new(ErrorKind::Other, e))?;
    println!("Flag: {}", flag);

    Ok(())
}
