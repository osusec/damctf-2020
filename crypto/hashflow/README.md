# hashflow

Description:

Why perform bounds checking when the data is authenticated?
Our super secure artisan hash function ensures that only trusted values can be input.

Flag:

```
dam{ArT1saN_H@5H-noT_SO-sECUrE}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
