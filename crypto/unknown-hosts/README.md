# unknown_hosts

Description:

Alice has a bad habit of using flags as her passwords.
After compromizing a router, you log some packets and find that she has an account on Bob's server (`ssh {{ challenge.expose.container1[0].host }} -p {{ challenge.expose.container1[0].tcp }}`).
You decide to try a man-in-the-middle attack.
You set the router so that when you make a TCP connection to it, with `socat TCP:{{ challenge.expose.container2[0].host }}:{{ challenge.expose.container2[0].tcp }} TCP:localhost:22`, her packets bound for Bob's server get redirected to your own SSH server.
But she only connects to `known_hosts`.

Flag:

```
dam{A_7rue_FRIeND-1$-kNoWN-by_THe1R_pubLIC_k3Y}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
