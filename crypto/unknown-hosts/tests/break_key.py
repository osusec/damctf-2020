#!/usr/bin/env python3

from Crypto.PublicKey import ECC
from Crypto.Util.number import *
from sage.all_cmdline import GF, EllipticCurve
from sage.groups.generic import discrete_log_lambda

key_bits = 44

a = 61503427459590845772843778506756552545461126364528813320849772547097162754542
c = 95472752562332475581110515377176489706227509488044797183494032664369500144789
n = 115792089210356248762697446949407573529996955224135760342422259061068512044369

class Rng:
    def __init__(self, seed):
        self.s = seed
    def gen(self):
        self.s = (a * self.s + c) % n
        return self.s

def gen_key(seed):
    rng = Rng(seed)
    k = 0
    for i in range(0, 256, 32):
        k += rng.gen() * 2**i
    return k % n

gen_key_b = gen_key(0)
gen_key_a = (gen_key(1) - gen_key_b) % n

p = 2**256 - 2**224 + 2**192 + 2**96 - 1
curve_a = -3
curve_b = 0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b
gx = 0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296
gy = 0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5
F = GF(p)
E = EllipticCurve(F, [curve_a, curve_b])
g = E(gx, gy)

with open("ssh_host_ecdsa_key.pub", "r") as f:
    key = ECC.import_key(f.read())
pub = E(int(key.pointQ.x), int(key.pointQ.y))

seed = discrete_log_lambda(pub - gen_key_b * g, gen_key_a * g, (0, 2**key_bits), operation='+')
print(seed)

k = gen_key(seed)
with open("ssh_host_ecdsa_key", "w") as f:
    f.write(ECC.construct(curve='NIST P-256', d=k).export_key(format='PEM'))
