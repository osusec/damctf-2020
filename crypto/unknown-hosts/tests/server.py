#!/usr/bin/env python3

import sys
import paramiko
import socket
import threading

HOST_KEY = paramiko.ecdsakey.ECDSAKey(filename="ssh_host_ecdsa_key")


class Server(paramiko.ServerInterface):
    def __init__(self):
        self.event = threading.Event()

    def check_channel_request(self, kind, chanid):
        if kind == 'session':
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_password(self, username, password):
        with open("password.out", 'w') as f:
            f.write(f"username: {username}, password {password}")

        return paramiko.AUTH_SUCCESSFUL


def main():
    if not len(sys.argv[1:]):
        print('Usage: ssh_server.py <SERVER>  <PORT>')
        return    # Create a socket object.

    server = sys.argv[1]
    ssh_port = int(sys.argv[2])

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((server, ssh_port))
        sock.listen(100)
        print('[+] Listening for connection ...')
        client, addr = sock.accept()
    except Exception as e:
        print(f'[-] Connection Failed: {str(e)}')
        return

    print('[+] Connection Established!')    # Creating a paramiko object.

    try:
        Session = paramiko.Transport(client)
        Session.add_server_key(HOST_KEY)
        server = Server()
        try:
            Session.start_server(server=server)
        except paramiko.SSHException as x:
            print('[-] SSH negotiation failed.')
            return

        session = Session.accept(10)
        print('[+] Authenticated!')

    except Exception:
        try:
            session.close()
        except Exception:
            pass


if __name__ == '__main__':
    main()
