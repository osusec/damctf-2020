#!/usr/bin/env python3

from Crypto.PublicKey import ECC
from Crypto.Util.number import *

key_bits = 44

a = 61503427459590845772843778506756552545461126364528813320849772547097162754542
c = 95472752562332475581110515377176489706227509488044797183494032664369500144789
n = 115792089210356248762697446949407573529996955224135760342422259061068512044369

class Rng:
    def __init__(self, seed):
        self.s = seed
    def gen(self):
        self.s = (a * self.s + c) % n
        return self.s

seed = getRandomInteger(key_bits)
rng = Rng(seed)
k = 0
for i in range(0, 256, 32):
    k += rng.gen() * 2**i
k = k % n

key = ECC.construct(curve='NIST P-256', d=k)

with open("ssh_host_ecdsa_key", "w") as f:
    f.write(key.export_key(format='PEM'))
with open("ssh_host_ecdsa_key.pub", "w") as f:
    f.write(key.public_key().export_key(format='OpenSSH'))
