#!/usr/bin/python3

import socket
import threading
import paramiko


class Server:
    def __init__(self, host="0.0.0.0", port=31337):
        with open('flag', 'r') as f:
            self.flag = f.read()

        print(f"Binding to {host}:{port}")
        self.s = socket.socket()
        self.s.bind((host, port))

    def listen(self):
        self.s.listen(5)
        while True:
            c, addr = self.s.accept()
            threading.Thread(target=self.puzzle, args=(c,)).start()

    def spawn_ssh_client(self, conn):
        client = paramiko.SSHClient()
        client.load_host_keys("./key")
        try:
            client.connect(
                "localhost",
                port=2222,
                username="root",
                password=self.flag,
                look_for_keys=False,
                allow_agent=False,
                sock=conn,
                banner_timeout=200
            )

        except (paramiko.SSHException, socket.error, EOFError) as e:
            pass
        else:
            client.close()

    def puzzle(self, conn):
        self.spawn_ssh_client(conn)
        conn.close()


if __name__ == "__main__":
    server = Server()
    server.listen()
