# side-channel-1

Description:

```
This is a very simple side-channel attack challenge.
The program generates a random password (8-hex digit),
and it has a routine that time.sleep(hex_ord(password[i])*0.1)
if the user sends a wrong character to the password position 'i'.
hex_ord() here returns the position of the hex character in order,
e.g., '0' => 0, '9' => 9, 'a' => 10, 'f' => 15.

Based on the timing difference in sending output messages
from the checker program, challengers may learn the correct password
and guess it right on the 2nd trial.
```

Flag:

```
dam{d0nT_d3l4y_th3_pRoC3sSiNg}
```

## Checklist for Author

* [+] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
