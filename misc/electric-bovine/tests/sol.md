# Solving Electric Bovine

1. Get source code from `!about`
2. Read source code and figure out that
	2.1. can only run `!role_add x y` in guild
	2.2. can use `!send_msg x` to send a command to guild.
3. Examining source code shows that authentication set combinations can be tricked by changing your in-guild nick to $private_role_id + 4 * str(discriminator). I.e.: private role is `763128087226351638`, and your discriminator is #`1195`, you change nick to `763128087226351638 + 1195119511951195 = 764323206738302833`. 
4. run `!send_msg !role_add ***630534967964860436* ***763128087226351638*`, in dms, where `*` is anything. This will grant the private permission (`763128087226351638`) to user (`630534967964860436`) if nick is set right.
5. Now you can use `!cowsay`. Examining the denylist shows that the only allowed characters are A-Z, a-z, <, >. 
6. Run `!cowsay <flag` in dms, to get flag.
