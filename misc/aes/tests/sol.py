#!/usr/bin/python3
import subprocess
from pwn import *
import time
from base64 import b64encode, b64decode
from Crypto.Random import get_random_bytes

def pow(sec_level, prefix):
    while(1):
        suffix = get_random_bytes(16)
        digest = hashlib.sha256(prefix + suffix).digest()
        if(digest[0:sec_level] == b'\x00' * sec_level):
            return b64encode(suffix).decode("utf-8")


#p = process(["/usr/bin/python3", "./chal.py"])
p = remote("chals.damctf.xyz", 30888)
n_req = 0

def get_words():
    words = str.split(subprocess.check_output("cat /usr/share/dict/american-english", shell=True).decode())
    return words
"""
print(p.recvuntil("POW PREFIX: "))
prefix = b64decode(p.recvuntil("\n"))
print(prefix)
p.sendline(pow(3, prefix))
"""
words = get_words()
p.recvuntil("MENU")

def get_response(payload):
    print("sending payload")
    global n_req
    n_req += 1
    print("num requests " + str(n_req))
    p.sendline("1")
    p.send(payload)
    p.send("\n\n")
    p.recvuntil("---BEGIN RESPONSE---\n")
    return p.recvuntil("---END RESPONSE---")[:-18]


queue = []

def binsearch(l, r, words):
    if(l == r): return
    if len(queue) >= 10:
        return 
    print("recur", l, r)
    res = get_response("\n".join(words[l:r]))
    #print(res)
    if(res):
        if r == l+1:
            print("FOUND")
            queue.append(l)
            print(queue)        
            return
        m = (r + l) // 2 
        binsearch(l, m, words)
        binsearch(m, r, words)

binsearch(0, len(words), words)

submission = " ".join([words[index] for index in queue])

p.recv()
p.sendline("3")
p.recv()
p.sendline(submission)
print(p.recv())

