# Solving pretty-good

 - Make a reverse image search for the provided image in TinEye, and find that a URL from https://rayhaanhodgson.com/ is linked.
 - Read blog https://rayhaanhodgson.com/2020/10/05/one-fish-two-phish/. This is because one fish two phish references deleted post that links to git
 - Use archive.org to return a result for https://rayhaanhodgson.com for the post "Work Work Work"
 - Verify that we can link to the work repo from the archive, or at least glean the github username from it.
 - GPG key signature `D66D33D350AAB609` can be collected from https://github.com/rhodgson-work/rickrollrc/commit/c43bda5bd7addddd1f905e67012c2e03c7f259ac or https://github.com/rhodgson-work/pytube/commit/fdefb4bddfa328ec68746cd9eff9c56feb396057 or https://github.com/rhodgson-work/pytube/commit/bdf77aa3a073fd8c3d0838c2d41dea77bb8a09c6
 - Searching keybase for `D66D33D350AAB609` returns user https://keybase.io/fortnite_and_wp.
 - User https://keybase.io/fortnite_and_wp is verified with a Gist on a github account https://github.com/r4yh44nh0dgs0n.
 - Go to repository https://github.com/r4yh44nh0dgs0n/dotfiles
 - Find IP in commit history at https://github.com/r4yh44nh0dgs0n/dotfiles/commit/6da3908cf1d988dec5d7d8676c5afc2ffb3715ee#diff-1a98577e6d2317695046014af9d963ddL4

