# pretty-good

Description:

```
Description here please
```

Flag:

```
dam{182_24_89_5}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
