# bezout2shell

Description:

```
You can jump anywhere you like if you do the Bézout.
But can you make your code small enough?
```

Flag:

```
dam{H0w_1oW_c7N_20U_b3Z0uT4u}
```

## Checklist for Author

* [X] Challenge compiles
* [X] Dockerfile has been updated
* [X] `container_src/run_chal.sh` has been updated
* [X] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [X] There is at least one test exploit/script in the `tests` directory

## Info

Distribute source, binary, and libc.
