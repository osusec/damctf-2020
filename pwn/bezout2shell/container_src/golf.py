#!/usr/bin/env python3

import datetime
import os
from rctf import golf

rctf_host = 'https://damctf.xyz'
challenge_id = 'bezout2shell'
ctf_start = 1602288000 # 2020-10-10T00:00:00Z
# ctf_start = 1602028800 # 2020-10-7T00:00:00Z

current_hours = golf.calculate_limit(rctf_host, challenge_id, ctf_start, lambda x: x)
os.environ["HOURS_SINCE_START"] = str(current_hours)
os.execl('./bezout2shell', './bezout2shell')
