# allokay

Description:

```
ROP + using '-' to skip canary. Calling win("/bin/sh").
```

Flag:

```
dam{4Re_u_A11_0cK4y}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
* [x] There is at least one test exploit/script in the `tests` directory
