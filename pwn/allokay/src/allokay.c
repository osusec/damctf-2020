#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <unistd.h>

char buffer[50];

void win(char* s) {
	execve(s, NULL, NULL);
}

void get_input(int sz) {
    int i;
    long* buffer = (long *) alloca(sz);

    for (i = 0; i < sz; i++) {
        printf("Enter number %d/%d: ", i, sz);
        scanf("%ld", &buffer[i]);
    }
}

int main() {
    printf("How many favorite numbers do you have?\n");
    fgets(buffer, 20, stdin);

    int rating = atoi(buffer);
    if (rating > 100 || rating <= 0) {
        printf("Huh?\n");
        exit(0);
    }

    printf("Please enter the numbers!\n");
    get_input(rating);

    return 0;
}
