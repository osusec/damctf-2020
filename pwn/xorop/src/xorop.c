#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

unsigned long key;
unsigned long b2ul = 0x0101010101010101;

int read_input();
static inline void encrypt_libc_addrs();;

int main()
{
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);

	puts("Hello. Please help me seed my RNG.");
	puts("Press any key to continue.");
	unsigned char c = getchar();

	srand(time(NULL) ^ (c << 16));
	key = (unsigned long) rand();
	key ^= (unsigned long) rand() << 21;
	key ^= (unsigned long) rand() << 42;

	void (*exit_)(int) = exit;
	// Force exit to load before encryption, and force it to be on the stack.
	asm volatile("" : "+m" (exit_) ::);

	read_input();

	exit_(EXIT_SUCCESS);
}

__attribute__((noinline))
void memfrob(void* s, unsigned char c, size_t n)
{
	unsigned long u = b2ul * (unsigned long) c;

	char* x = s;
	char* y = x + n;
	for (; x < y; x += 8)
		*(unsigned long*) x ^= u;
}

int read_input()
{
	char buf[0100];

	puts("What string would you like to memfrob?");
	fgets(buf, 0x100, stdin);

	size_t len = strlen(buf);
	memfrob(buf, 42, len);
	buf[len] = 0;
	puts("Your string, memfrobbed:");
	puts(buf);

	puts("closing stdout: no leaks 4 u.");
	if (dup(STDOUT_FILENO) == -1 || close(STDOUT_FILENO))
		exit(EXIT_FAILURE);

	encrypt_libc_addrs();

	// Don't give out free values in registers.
	asm volatile("xor %%ecx, %%ecx\n\txor %%edx, %%edx\n\txor %%esi, %%esi\n\txor %%r10d, %%r10d"
	             ::: "rcx", "rdx", "rsi", "r10");

	return 0;
}

static void encrypt_libc_addrs()
{
	char* start = (char*) 0x600000;
	char* end = (char*) 0x601000 - sizeof(unsigned long);

	// Force into register to avoid rop gadgets.
	asm("" : "+r" (end) ::);

	unsigned long libc_addr = (unsigned long) puts;
	unsigned int libc_addr_high = libc_addr >> 24;

	for (; start <= end; ++start)
	{
		if (*(unsigned int*) (start + 3) == libc_addr_high)
		{
			*(unsigned long*) start += key;
			start += sizeof(unsigned long) - 1;
		}
	}
}

// Put __libc_csu_init back in because we need its ROP gadgets. -nostdlib removed it.
void __libc_csu_init()
{
	asm volatile(
	"\n\tpush   %r15"
	"\n\tpush   %r14"
	"\n\tmov    %rdx,%r15"
	"\n\tpush   %r13"
	"\n\tpush   %r12"
	"\n\tlea    key(%rip),%r12"
	"\n\tpush   %rbp"
	"\n\tlea    b2ul(%rip),%rbp"
	"\n\tpush   %rbx"
	"\n\tmov    %edi,%r13d"
	"\n\tmov    %rsi,%r14"
	"\n\tsub    %r12,%rbp"
	"\n\tsub    $0x8,%rsp"
	"\n\tsar    $0x3,%rbp"
	"\n\tcallq  main"
	"\n\ttest   %rbp,%rbp"
	"\n\tje     2f"
	"\n\txor    %ebx,%ebx"
	"\n\tnopl   0x0(%rax,%rax,1)"
	"\n\t1: mov    %r15,%rdx"
	"\n\tmov    %r14,%rsi"
	"\n\tmov    %r13d,%edi"
	"\n\tcallq  *(%r12,%rbx,8)"
	"\n\tadd    $0x1,%rbx"
	"\n\tcmp    %rbx,%rbp"
	"\n\t2: jne    1b"
	"\n\tadd    $0x8,%rsp"
	"\n\tpop    %rbx"
	"\n\tpop    %rbp"
	"\n\tpop    %r12"
	"\n\tpop    %r13"
	"\n\tpop    %r14"
	"\n\tpop    %r15");
}
