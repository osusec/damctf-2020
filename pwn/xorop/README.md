# xorop

Description:

It's no wonder you could solve addrop when it gave you *so* many gadgets.

Note: Our solution takes about 30 connections and runs within a minute on average. Please don't DOS our server with hundreds of queries.

Flag:

```
dam{X0r1N6-m3aN1N913S5-D4Ta:_7he_deFiN1tIve_6UIdE_T0_CTF}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
