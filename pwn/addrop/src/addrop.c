#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

unsigned long key;
unsigned long b2ul = 0x0101010101010101;

int read_input();
static inline void encrypt_libc_addrs();;

int main()
{
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);

	srand(time(NULL));
	key = (unsigned long) rand();
	key ^= (unsigned long) rand() << 21;
	key ^= (unsigned long) rand() << 42;

	void (*exit_)(int) = exit;
	// Force exit to load before encryption, and force it to be on the stack.
	asm volatile("" : "+m" (exit_) ::);

	read_input();

	exit_(EXIT_SUCCESS);
}

__attribute__((noinline))
void memfrob(void* s, unsigned char c, size_t n)
{
	unsigned long u = b2ul * (unsigned long) c;

	char* x = s;
	char* y = x + n;
	for (; x < y; x += 8)
		*(unsigned long*) x ^= u;
}

int read_input()
{
	char buf[0100];

	puts("Hello.");
	puts("What string would you like to memfrob?");
	fgets(buf, 0x100, stdin);

	size_t len = strlen(buf);
	memfrob(buf, 42, len);
	buf[len] = 0;
	puts("Your string, memfrobbed:");
	puts(buf);

	puts("closing stdout: no leaks 4 u.");
	if (dup(STDOUT_FILENO) == -1 || close(STDOUT_FILENO))
		exit(EXIT_FAILURE);

	encrypt_libc_addrs();

	// Don't give out free values in registers.
	asm volatile("xor %%ecx, %%ecx\n\txor %%edx, %%edx\n\txor %%esi, %%esi\n\txor %%r10d, %%r10d"
	             ::: "rcx", "rdx", "rsi", "r10");

	return 0;
}

static void encrypt_libc_addrs()
{
	char* start = (char*) 0x600000;
	char* end = (char*) 0x601000 - 8;

	unsigned long libc_addr = (unsigned long) puts;
	unsigned int libc_addr_high = libc_addr >> 24;

	for (; start <= end; ++start)
	{
		if (*(unsigned int*) (start + 3) == libc_addr_high)
		{
			*(unsigned long*) start += key;
			start += 7;
		}
	}
}
