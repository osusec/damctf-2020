# Ghostbusters

Description:

```
If you're seeing things
Running through your head
It's time to call
Ghostbusters!
```

Flag:

```
dam{H4uNt3d_8y_7h3_gH057S_0f_t1M3}
```

## Checklist for Author

* [X] Challenge compiles
* [X] Dockerfile has been updated
* [X] `container_src/run_chal.sh` has been updated
* [X] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [X] There is at least one test exploit/script in the `tests` directory

## Info

Only give out the `ghostbusters` binary.
