#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

int main()
{
	volatile char haunted[sizeof(time_t)] = {0};
	void (*fun)();

	printf("Who you gonna call?\n");
	if (scanf("%p", &fun) != 1)
		return EXIT_FAILURE;

	asm("movb $0x6e, (%%rdi)" ::
		"D" (&haunted[0]) :
		"memory", "rsi");

	if (fun)
		fun();

	switch (haunted[0])
	{
	case 'n':
		printf("I ain't afraid of no ghost!\n");
		break;
	case 'y':
		execl("/bin/sh", "sh", NULL);
		break;
	default: ;
	}

	return EXIT_SUCCESS;
}
