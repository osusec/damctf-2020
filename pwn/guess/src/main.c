#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>
#include <unistd.h>

extern unsigned char flag_hash[];

int main() {
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);

	puts("If you're so good at CTF, why don't you just guess the flag?");
	puts("I'll give you ten tries.");

	char* line_buffer = NULL;
	size_t line_buffer_size = 0;

	for (int i = 0; i < 10; ++i)
	{
		ssize_t bytes_read = getline(&line_buffer, &line_buffer_size, stdin);
		if (bytes_read <= 0)
			exit(EXIT_FAILURE);
		if (line_buffer[bytes_read - 1] == '\n')
		{
			line_buffer[bytes_read - 1] = 0;
			--bytes_read;
		}

		unsigned char digest[SHA256_DIGEST_LENGTH];
		if (SHA256(line_buffer, bytes_read, digest) != digest)
			exit(EXIT_FAILURE);

		if (memcmp(digest, flag_hash, SHA256_DIGEST_LENGTH) == 0)
		{
			printf("Correct!\n");
			execl("/bin/sh", "sh", NULL);
		}
		else
		{
			printf(line_buffer);
			printf(" was an incorrect guess\n");
		}
	}

	free(line_buffer);
	return EXIT_SUCCESS;
}
