# guess

Description:

```
You've proven yourself a master of DamCTF. Now put your newly learned skills to work by guessing the flag in its entirety!
```

Flag:

```
dam{bWahAHAh@ha-You'L1-neveR-9uES$_+H15-N0T_1n-4_M|1110n-yEaR5!}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory

## Info

_Author, put whatever you want here_
